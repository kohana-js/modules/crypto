# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.0.4](https://gitlab.com/kohana-js/modules/crypto/compare/v3.0.3...v3.0.4) (2022-09-07)

### 3.0.3 (2022-03-09)

## [3.0.2] - 2021-09-07
### Added
- create CHANGELOG.md